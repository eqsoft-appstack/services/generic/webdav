#!/bin/sh

clean_up() {
  log 6 "clean up container and volumes"
  docker rm -f $test_container_name
  docker volume rm ${test_name}-data
  docker volume rm ${test_name}-tmp_data
}

cp -r "${tests_folder}/auth" "${test_cache_folder}/auth"
cp -r "${tests_folder}/sites-available" "${test_cache_folder}/sites-available"

docker volume create ${test_name}-data
docker volume create ${test_name}-tmp_data

# edit your test conditions
ret=$(docker run -d --rm \
  --name $test_container_name \
  -v "${test_cache_folder}/sites-available:/etc/nginx/sites-available" \
  -v "${test_cache_folder}/auth:/etc/nginx/auth" \
  -v ${test_name}-data:/var/www/data \
  -v ${test_name}-tmp_data:/tmp_data \
  -p ${free_port}:80 \
  $test_image)

log 6 "webdav module:"

docker exec $test_container_name nginx -V 2>&1 | tr ' ' '\n' | grep -E -i 'http_dav_module|http-dav-ext'

log 6 "check unauthorized access:"

curl -s -I http://$build_host:${free_port}/data | grep -i '401 Unauthorized'

log 6 "check PUT test file to webdav with authorized access:"

curl -s -T  ${tests_folder}/sites-available/default -u 'webdav:webdav' http://$build_host:${free_port}/data/default

log 6 "GET PROPFIND file:"

display_name=$(curl -s --request PROPFIND -u 'webdav:webdav' http://$build_host:${free_port}/data/default | grep '<D:displayname>default</D:displayname>')

if [ -z "${display_name}" ]; then
  log 3 "could not get PROPFIND request from test file"
  clean_up
  $(exit 1)
fi

log 6 "PROPFIND display_name: $display_name"
