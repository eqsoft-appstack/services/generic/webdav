#!/bin/sh

# edit your test conditions
ret=$(docker run -d --rm --name $test_container_name -p ${free_port}:80 $test_image)

echo "docker run test_image return: $ret"

sleep 3

docker logs $test_container_name

ret=$(docker exec $test_container_name nginx -V)

echo $ret

ret=$(curl --silent -m 5 "http://${build_host}:${free_port}/" | grep "Welcome to nginx")

#ret=$(docker exec $test_image curl --silent -m 5 http://127.0.0.1 | grep "Welcome to nginx")

if [ "$?" != "0" ] || [ -z "$ret" ] ; then
  log 3 "testing image not successfull!"
  exit 1
fi

log 5 "testing image successfull"
